#include "circlemanager.h"

#include <QDebug>
CircleManager::CircleManager(QObject *parent) : QObject(parent)
{

}

void CircleManager::setCircleModel(CircleModel *model)
{
    Q_CHECK_PTR( model );
    m_model = model;
}

CircleManager::circleClicked(int index)
{
    qDebug() << index;

    if( m_model->data( m_model->index(index), CircleModel::CircleColorRole ).value<QColor>() != QColor("red") )
    {
        m_model->setData( m_model->index(index), QColor("red"), CircleModel::CircleColorRole );
    }
    else
    {
        m_model->removeRow( index );
    }
}
