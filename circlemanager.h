#ifndef CIRCLEMANAGER_H
#define CIRCLEMANAGER_H

#include <QObject>

#include "circlemodel.h"

class CircleManager : public QObject
{
    Q_OBJECT
public:
    explicit CircleManager(QObject *parent = 0);

    void setCircleModel( CircleModel* model);

    Q_INVOKABLE circleClicked( int index );

private:

    CircleModel* m_model;
};

#endif // CIRCLEMANAGER_H
