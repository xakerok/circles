#include "circle.h"

#include <time.h>
#include <random>

Circle::Circle()
{
    m_radius = 10;
    m_centerPoint.setX( rand()%400 + m_radius );
    m_centerPoint.setY( rand()%400 + m_radius );

    m_color.setRgb( rand()%255, rand()%255, rand()%255 );
}

QPoint Circle::centerPoint() const
{
    return m_centerPoint;
}

int Circle::radius() const
{
    return m_radius;
}

void Circle::updateCenterPoint()
{
    m_centerPoint.setX( m_centerPoint.x() + rand()%3 - 1 );
    m_centerPoint.setY( m_centerPoint.y() + rand()%3 - 1 );
}

void Circle::changeColor(const QColor &color)
{
    m_color = color;
}

QColor Circle::color() const
{
    return m_color;
}
