#ifndef CIRCLEMODEL_H
#define CIRCLEMODEL_H

#include <QAbstractListModel>
#include <QTimer>
#include "circle.h"


class CircleModel : public QAbstractListModel
{
public:
    CircleModel(QObject* parent = nullptr );
    ~CircleModel();

    int rowCount(const QModelIndex &parent) const;
    QVariant data(const QModelIndex &index, int role) const;

    bool setData(const QModelIndex &index, const QVariant &value, int role);

    bool removeRows(int row, int count, const QModelIndex &parent);

   typedef enum
    {
        CircleCenterRole = Qt::UserRole + 1,
        CircleRadiusRole,
        CircleXRole,
        CircleYRole,
        CircleColorRole
    } TECircleRoles;

    QHash<int,QByteArray> roleNames() const;


private:
    QTimer* m_timer;

    QList<Circle*> m_list;


};

#endif // CIRCLEMODEL_H
