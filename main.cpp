#include <QGuiApplication>
#include <QQmlApplicationEngine>
#include <QQmlContext>

#include "circlemodel.h"
#include "circlemanager.h"

int main(int argc, char *argv[])
{
    QGuiApplication app(argc, argv);

    CircleManager manager;
    CircleModel model;
    manager.setCircleModel( &model );

    QQmlApplicationEngine engine;

    engine.rootContext()->setContextProperty( "CircleModel", &model );
    engine.rootContext()->setContextProperty( "CircleManager", &manager );

    engine.load(QUrl(QStringLiteral("qrc:/main.qml")));

    return app.exec();
}
