#ifndef CIRCLE_H
#define CIRCLE_H

#include <QPoint>
#include <QColor>

class Circle
{
public:
    Circle();

    QPoint centerPoint() const;
    int radius() const;

    void updateCenterPoint();

    void changeColor( const QColor& color );
    QColor color() const;

private:
    int m_radius;
    QPoint m_centerPoint;

    QColor m_color;

};

#endif // CIRCLE_H
