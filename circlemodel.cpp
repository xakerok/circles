#include "circlemodel.h"
#include <time.h>
#include <QDebug>

CircleModel::CircleModel(QObject *parent): QAbstractListModel( parent )
{
    srand( time( nullptr ) );

    m_timer = new QTimer( this );

    connect( m_timer, &QTimer::timeout,
             this, [this]()
    {
        beginResetModel();

        foreach (Circle* c, m_list) {
            c->updateCenterPoint();
        }
        endResetModel();
    });


    for( int i=0; i<5; i++ )
        m_list.append( new Circle );

    m_timer->start( 1000 );

}

CircleModel::~CircleModel()
{
    qDeleteAll(m_list);
}

int CircleModel::rowCount(const QModelIndex &parent) const
{
    return m_list.size();
}

QVariant CircleModel::data(const QModelIndex &index, int role) const
{
    if( index.isValid() )
    {
        if( role == CircleCenterRole )
            return m_list[index.row()]->centerPoint();

        if( role == CircleXRole )
            return m_list[index.row()]->centerPoint().x() - m_list[index.row()]->radius();

        if( role == CircleYRole )
            return m_list[index.row()]->centerPoint().y() - m_list[index.row()]->radius();

        if( role == CircleRadiusRole )
            return m_list[index.row()]->radius();

        if( role == CircleColorRole )
            return m_list[index.row()]->color();

    }
    return QVariant();

}

bool CircleModel::setData(const QModelIndex &index, const QVariant &value, int role)
{
    if( role == CircleColorRole && index.isValid() )
    {
        m_list[index.row()]->changeColor( value.value<QColor>() );
        Q_EMIT dataChanged( index, index, QVector<int>( CircleColorRole) );
        return true;
    }
    return false;
}

bool CircleModel::removeRows(int row, int count, const QModelIndex &parent)
{
    beginRemoveRows( parent, row, row + count - 1);

    m_list.removeAt( row );

    endRemoveRows();

    return true;
}


QHash<int, QByteArray> CircleModel::roleNames() const
{
    QHash<int, QByteArray> hash;

    hash.insert( CircleCenterRole, "CircleCenter" );
    hash.insert( CircleRadiusRole, "CircleRadius" );

    hash.insert( CircleXRole, "CircleX" );
    hash.insert( CircleYRole, "CircleY" );

    hash.insert( CircleColorRole, "CircleColor" );

    return hash;
}



