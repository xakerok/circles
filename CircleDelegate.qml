import QtQuick 2.5
import QtQuick.Controls 1.4

Rectangle {
    id: rect

    color: CircleColor

    width: CircleRadius * 2
    height: CircleRadius * 2

    radius: CircleRadius

    x: CircleX
    y: CircleY

    MouseArea {
        anchors.fill: parent
        onClicked: { CircleManager.circleClicked( index ); }
    }
}
